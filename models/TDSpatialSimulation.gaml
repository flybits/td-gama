/**
* Name: TDSpatialSimulation
* Author: farzin
* Description: 
* Tags: Tag1, Tag2, TagN
*/

model TDSpatialSimulation


/* Insert your model definition here */

global {
	
	shape_file shape_city <- shape_file("../includes/Route_Regional_Road.shp") parameter: "shape";
	
	
	
//	shape_file shape_city <-shape_file("../includes/Buildings_TO_WGS84.shp");//	shape_file("../includes/2018_Vacant_Lands_with_PropertyPINS.shp");//shape_file("../includes/Route_Regional_Road.shp");//
    geometry shape <- envelope(shape_city);
    string cityIOurl <-"http://localhost:8080";
    map<string, unknown> matrixData;
    string userId;
    int deviceType;
    int initOffset;
    int tdpeople <- 0;
    int cibcpeople <- 0;
    int bmopeople <- 0;
    int thspeople <- 0;
    
    list<td> tdBranches <- [];    
    	matrix<string,unknown> csvData;
    image_file torontoMap <- image_file("../images/CITY-OF-TORONTO-map_rt.jpg");
    	int nbofTD;
    	int nbofCIBC;
    	int nbofBMO;
    	int nbofTHS;
    	//speed facet value
    	float spd;

	int day;
    	
    	list<string> j <- [];
    	
    	
	init {
		

		spd <- 15.0;

		day <- 0;  	
		
		create city from:shape_city;
//	  	do create_static_agent("tdDowntownToronto2.json", td);
//	  	do create_static_agent("bmoDowntownToronto2.json", bmo);
//	  	do create_static_agent("cibcDowntownToronto2.json", cibc);
//	  	do create_static_agent("thsDowntownToronto2.json", timHortons);
	  	
	  	
	  	do createTDs;
	  	do createBMO;
	  	do createCIBC;
	  	do createTimHortons;
	  	
	  	
	  	
//	  	j <- remove_duplicates(world.members accumulate (species(each).name));
//	  	species k <- species(j[0]);
	  	
	  	

	  	//do create_static_agent("kagdfsadjkhf",j[1]);
	  	
		
	}
	
	
//	action create_static_agent(string location_file,  string a){
//		map<string, unknown> cityMatrixData;
//		string cityIOUrl <- "../includes/" + location_file;//tdDowntownToronto.json";	
//		list<map<string, unknown>> cityMatrixCell;
//		list<float> density_array;
//		list<float> current_density_array;	
//		float lng;
//		float lat;		
//			cityMatrixData <- json_file(cityIOUrl).contents;
//			cityMatrixCell <- cityMatrixData["candidates"];
//
//				
//			loop l over: cityMatrixCell { 
//				lng <- float(l["location"]["x"]);
//				lat  <- float(l["location"]["y"]);
//
//				create td{
//					name <- l["attributes"]["Place_addr"];
//					loc <- point(to_GAMA_CRS({lng, lat}, "EPSG:4326"));
//					location <- loc;
//				}
//			}		
//	}
	
	action createTDs{

		map<string, unknown> buildingMatrixData;
		string building_json_file <- "../includes/tdYorkRegion_combined.json";
		//"../includes/tdMississauga_combined.json";
		//"../includes/tdDowntownToronto_combined.json";//"../includes/tdDowntownToronto2.json";//"../includes/tdbranchesmississauga.json";//"../includes/tdRichmondhill.json";//"../includes/tdDowntownToronto2.json";	
		list<map<string, unknown>> cityMatrixCell;
		list<float> density_array;
		list<float> current_density_array;	
		float lng;
		float lat;		
		buildingMatrixData <- json_file(building_json_file).contents;
		cityMatrixCell <- buildingMatrixData["candidates"];
		cityMatrixCell <- remove_duplicates(cityMatrixCell);
			
			loop l over: cityMatrixCell { 
				lng <- float(l["location"]["x"]);
				lat  <- float(l["location"]["y"]);

				create td{
					name <- l["attributes"]["Place_addr"];
					loc <- point(to_GAMA_CRS({lng, lat}, "EPSG:4326"));
					location <- loc;
				}
			}
	}
//	action createTDs{
//	
//		map<string, unknown> cityMatrixData;
//		string cityIOUrl <- "../includes/tdbranchesdowntown.json";	//"../includes/tdbranchesmississauga.json";//
//		list<map<string, unknown>> cityMatrixCell;
//		list<float> density_array;
//		list<float> current_density_array;	
//		float lng;
//		float lat;		
//			cityMatrixData <- json_file(cityIOUrl).contents;
//			cityMatrixCell <- cityMatrixData["results"];
//
//			
//			loop l over: cityMatrixCell { 
//				lng <- float(l["geometry"]["location"]["lng"]);
//				lat  <- float(l["geometry"]["location"]["lat"]);
//
//				create td{
//					name <- l["formatted_address"];
//					loc <- point(to_GAMA_CRS({lng, lat}, "EPSG:4326"));
//					//loc <- point({lng,lat});
//					location <- loc;
//				}
//				
//			}
//	}
	
	action createBMO{
		map<string, unknown> cityMatrixData;
		string cityIOUrl <- "../includes/bmoYorkRegion_combined.json";
		//"../includes/bmoMississauga_combined.json";
		//"../includes/bmoDowntownToronto_combined.json";//"../includes/bmoDowntownToronto2.json";//"../includes/bmobranchesmississauga.json";//"../includes/bmoRichmondhill.json";//"../includes/bmoDowntownToronto2.json";
		list<map<string, unknown>> cityMatrixCell;
		list<float> density_array;
		list<float> current_density_array;	
		float lng;
		float lat;		
		cityMatrixData <- json_file(cityIOUrl).contents;
		cityMatrixCell <- cityMatrixData["candidates"];
		cityMatrixCell <- remove_duplicates(cityMatrixCell);
			
			loop l over: cityMatrixCell { 
				lng <- float(l["location"]["x"]);
				lat  <- float(l["location"]["y"]);

				create bmo{
					name <- l["attributes"]["Place_addr"];
					loc <- point(to_GAMA_CRS({lng, lat}, "EPSG:4326"));
					location <- loc;
				}
				
			}			
	}
//	action createBMO{
//		map<string, unknown> cityMatrixData;
//		string cityIOUrl <- "../includes/bmobranchesdowntown.json";	//"../includes/bmobranchesmississauga.json";
//		list<map<string, unknown>> cityMatrixCell;
//		list<float> density_array;
//		list<float> current_density_array;	
//		float lng;
//		float lat;		
//			cityMatrixData <- json_file(cityIOUrl).contents;
//			cityMatrixCell <- cityMatrixData["results"];
//
//			
//			loop l over: cityMatrixCell { 
//				lng <- float(l["geometry"]["location"]["lng"]);
//				lat  <- float(l["geometry"]["location"]["lat"]);
//
//				create bmo{
//					name <- l["formatted_address"];
//					loc <- point(to_GAMA_CRS({lng, lat}, "EPSG:4326"));
//					//loc <- point({lng,lat});
//					location <- loc;
//				}
//				
//			}			
//	}		
	
	action createCIBC{
		map<string, unknown> cityMatrixData;
		string cityIOUrl <- "../includes/cibcYorkRegion_combined.json";
		//"../includes/cibcMississauga_combined.json";
		//"../includes/cibcDowntownToronto_combined.json";//"../includes/cibcDowntownToronto2.json";//"../includes/cibcbranchesmississauga.json";//"../includes/cibcRichmondhill.json";//"../includes/cibcDowntownToronto2.json";	
		list<map<string, unknown>> cityMatrixCell;
		list<float> density_array;
		list<float> current_density_array;	
		float lng;
		float lat;		
		cityMatrixData <- json_file(cityIOUrl).contents;
		cityMatrixCell <- cityMatrixData["candidates"];
		cityMatrixCell <- remove_duplicates(cityMatrixCell);
		
		loop l over: cityMatrixCell { 
			lng <- float(l["location"]["x"]);
			lat  <- float(l["location"]["y"]);

			create cibc{
				name <- l["attributes"]["Place_addr"];
				loc <- point(to_GAMA_CRS({lng, lat}, "EPSG:4326"));
				location <- loc;
			}
		}	
	}
//	action createCIBC{
//		map<string, unknown> cityMatrixData;
//		string cityIOUrl <- "../includes/cibcbranchesmississauga.json";//"../includes/tdbranchesdowntown.json";	
//		list<map<string, unknown>> cityMatrixCell;
//		list<float> density_array;
//		list<float> current_density_array;	
//		float lng;
//		float lat;		
//			cityMatrixData <- json_file(cityIOUrl).contents;
//			cityMatrixCell <- cityMatrixData["results"];
//
//			
//			loop l over: cityMatrixCell { 
//				lng <- float(l["geometry"]["location"]["lng"]);
//				lat  <- float(l["geometry"]["location"]["lat"]);
//
//				create cibc{
//					name <- l["formatted_address"];
//					loc <- point(to_GAMA_CRS({lng, lat}, "EPSG:4326"));
//					//loc <- point({lng,lat});
//					location <- loc;
//				}
//				
//			}				
//	}	
	
	action createTimHortons{
		map<string, unknown> cityMatrixData;
		string cityIOUrl <- "../includes/thsYorkRegion_combined.json";
		//"../includes/thsMississauga_combined.json";
		//"../includes/thsDowntownToronto_combined.json";//"../includes/thsDowntownToronto2.json";//"../includes/timhortonsmississauga.json";//"../includes/thsRichmondhill.json";//"../includes/thsDowntownToronto2.json";
		list<map<string, unknown>> cityMatrixCell;
		list<float> density_array;
		list<float> current_density_array;	
		float lng;
		float lat;		
		cityMatrixData <- json_file(cityIOUrl).contents;
		cityMatrixCell <- cityMatrixData["candidates"];
		cityMatrixCell <- remove_duplicates(cityMatrixCell);
		loop l over: cityMatrixCell { 
			lng <- float(l["location"]["x"]);
			lat  <- float(l["location"]["y"]);

			create timHortons{
				name <- l["attributes"]["Place_addr"];
				loc <- point(to_GAMA_CRS({lng, lat}, "EPSG:4326"));
				location <- loc;
			}			
		}
		write length(timHortons);

	}
//	action createTimHortons{
//		map<string, unknown> cityMatrixData;
//		string cityIOUrl <- "../includes/timhortonsmississauga.json";//"../includes/tdbranchesdowntown.json";	
//		list<map<string, unknown>> cityMatrixCell;
//		list<float> density_array;
//		list<float> current_density_array;	
//		float lng;
//		float lat;		
//			cityMatrixData <- json_file(cityIOUrl).contents;
//			cityMatrixCell <- cityMatrixData["results"];
//
//			
//			loop l over: cityMatrixCell { 
//				lng <- float(l["geometry"]["location"]["lng"]);
//				lat  <- float(l["geometry"]["location"]["lat"]);
//
//				create timHortons{
//					name <- l["formatted_address"];
//					loc <- point(to_GAMA_CRS({lng, lat}, "EPSG:4326"));
//					//loc <- point({lng,lat});
//					location <- loc;
//				}
//				
//			}
//
//	}
	
	reflex name:start when:cycle=0{
		day <- day + 1;
		do createPeople(day);
	}
	
	reflex name:repeat when:(cycle!=0 and (nbofTD=0 and nbofCIBC=0 and nbofBMO=0 and nbofTHS=0)){
		day <- day + 1;
		if day > 31{
			do pause;
		}
		else{
			do createPeople(day);
		}
		
	}
	
	action createPeople(int d){
		string monthday;
		string filename;
		int hour <- 0;
		write d;
				filename <- "DowntownToronto3_201808" + d + ".csv";//"Mississauga_201808" + d + ".csv";//"RichmondHill_201808" + d + ".csv";//
				write filename;
				create tdusers from:csv_file( "../results/TD/" + filename,true) with:
					[userid::string(get("userid")), 
						latitude::float(get("inbranch_location.y")), 
						longitude::float(get("inbranch_location.x")),
						initlat::float(get("initLocation.y")),
						initlng::float(get("initLocation.x"))
		
					]{
						location <- point({initlng, initlat});
						the_target <-  point(to_GAMA_CRS({longitude, latitude}, "EPSG:4326"));//
						nbofTD <- nbofTD + 1;
					}
				create bmousers from:csv_file( "../results/BMO/"+filename,true) with:
					[userid::string(get("userid")), 
						latitude::float(get("inbranch_location.y")), 
						longitude::float(get("inbranch_location.x")),
						initlat::float(get("initLocation.y")),
						initlng::float(get("initLocation.x"))
					]{
						location <- point({initlng, initlat});
						the_target <-  point(to_GAMA_CRS({longitude, latitude}, "EPSG:4326"));//
						nbofBMO <- nbofBMO + 1;
					}
				create cibcusers from:csv_file( "../results/CIBC/"+filename,true) with:
					[userid::string(get("userid")), 
						latitude::float(get("inbranch_location.y")), 
						longitude::float(get("inbranch_location.x")),
						initlat::float(get("initLocation.y")),
						initlng::float(get("initLocation.x"))						
		
					]{
						location <- point({initlng, initlat});
						the_target <-  point(to_GAMA_CRS({longitude, latitude}, "EPSG:4326"));//
						nbofCIBC <- nbofCIBC + 1;
					}	
				create thsusers from:csv_file( "../results/THS/"+filename,true) with:
					[userid::string(get("userid")), 
						latitude::float(get("inbranch_location.y")), 
						longitude::float(get("inbranch_location.x")),
						initlat::float(get("initLocation.y")),
						initlng::float(get("initLocation.x"))		
										
		
					]{

						location <- point({initlng, initlat});
						the_target <-  point(to_GAMA_CRS({longitude, latitude}, "EPSG:4326"));//
						nbofTHS <- nbofTHS + 1;
					}			
	}

}

species name:city schedules:[]{
	aspect base{
		draw shape color:#black;
	}
}


species name:building{

	int numberOfPeople;
	image_file my_icon;	
	geometry loc;
	int n_people;

	user_command people_inside_branch {
		numberOfPeople <- length(agents_at_distance(10.0#m) of_species people);				
		loc <- shape CRS_transform("EPSG:4326");
	}


	aspect base {		
		draw  my_icon size: 250.0;
	}
	
}

species name:td parent:building{

//	int peopleInsideThisBranch;
	image_file my_icon <- file("../includes/TD-bank-icon.png") ;	
//	geometry loc;
//	int n_people;
//
//	user_command people_inside_branch {
//		peopleInsideThisBranch <- length(agents_at_distance(10.0#m) of_species tdusers);				
//		loc <- shape CRS_transform("EPSG:4326");
//	}
//
//
//	aspect base {		
//		draw  my_icon size: 250.0;
//	}

}

species name:bmo parent:building{

//	int peopleInsideThisBranch;
	image_file my_icon <- file("../includes/bmo.jpg") ;
//	list<string> lp;
//	list<people> i;
//	geometry loc;
//
//	user_command peopleInsideThisBranch{
//		peopleInsideThisBranch <- length(agents_at_distance(10.0#m) of_species bmousers);		
//	}
//	
//	aspect name:base{
//		draw my_icon size: 250.0;
//	}	
}


species name:cibc parent:building{

//	string address;
//	int peopleInsideThisBranch;
	image_file my_icon <- file("../includes/cibc.jpg") ;
//	list<string> lp;
//	list<people> i;
//	geometry loc;
//	
//	user_command peopleInsideThisBranch{
//		peopleInsideThisBranch <- length(agents_at_distance(10.0#m) of_species cibcusers);		
//	}
//	
//	aspect name:base{
//		draw cibc_people size: 250.0;
//	}	
}


species name:timHortons parent:building{

//	int peopleInsideThisTimHorton;
//	list<string> lp;
	image_file my_icon <- file("../includes/timhortons.png") ;
//	list<people> i;
//	geometry loc;
//	
//	user_command peopleInsideThisTimHortons{
//		peopleInsideThisTimHorton <- length(agents_at_distance(10.0#m) of_species thsusers);		
//	}
//	
//	aspect name:base{
//		draw my_icon size: 225.0;
//	}
}


species tdusers parent:people {

	
	reflex name:move when:the_target != nil{
		do goto target: the_target  speed:spd;
		if location with_precision 5 = the_target with_precision 5{
			the_target <- nil;
			ask world{
				tdpeople <- tdpeople + 1;
				nbofTD <- nbofTD - 1;	
			}
			// to mke the simulation faster
			if day < 25 {
					do die;
			}
		}
	}
//	aspect default {
//		draw  my_icon size:325.0; //shape color:#black;//
//	}

}


species cibcusers parent:people {
	
	reflex name:move when:the_target != nil{
		do goto target: the_target  speed:spd;
		if location with_precision 5 = the_target with_precision 5{
			the_target <- nil;
			ask world{
				cibcpeople <- cibcpeople + 1;
				nbofCIBC <- nbofCIBC - 1;												
			}
			// to mke the simulation faster
			if day < 25 {
					do die;
			}
		}
	}
//	aspect default {
//		draw  my_icon size:325.0;
//	}
}

species bmousers parent:people {

	reflex name:move when:the_target != nil{
		do goto target: the_target  speed:spd;
		if location with_precision 5 = the_target with_precision 5{
			the_target <- nil;
			ask world{
				bmopeople <- bmopeople + 1;				
				nbofBMO <- nbofBMO -1;												
			}
			// to mke the simulation faster			
			if day < 25 {
					do die;
			}
		}
	}
//	aspect default {
//		draw  my_icon size:325.0; //shape color:#black;//
//	}
}

species thsusers parent:people {
	
	reflex name:move when:the_target != nil{
		do goto target: the_target  speed:spd;
		if location with_precision 5 = the_target with_precision 5{
			the_target <- nil;
			ask world{
				thspeople <- thspeople + 1;
				nbofTHS <- nbofTHS -1;												
			}
			// to mke the simulation faster
			if day < 25 {
					do die;
			}
		}
	}
//	aspect default {
//		draw  my_icon size:325.0;
//	}
}


species name:people skills:[moving] {
	float longitude;
	float latitude;
	string userid;
	image_file my_icon <- file("../includes/person.png") ;
	point the_target;
	city building;
	int continue <- 1;
	float initlat;
	float initlng;
	
	aspect default {
		draw  my_icon size:325.0; //shape color:#black;//
	}
		
}


experiment main type: gui{
	
	//user_command "Athena" action:getAthenaData;

	output {
		display name:map refresh_every:1{
			//Toronto
			//image ("../images/CITY-OF-TORONTO-map_rt.jpg") refresh: false;
			
			//Mississauga
			//image ("../images/mississauga1.png") refresh: false;
			
			
			species city aspect:base;

			species td aspect:base;
			species bmo aspect:base;
			species cibc aspect:base;	
			species 	timHortons aspect:base;	
			species tdusers;
			species cibcusers;
			species bmousers;
			species thsusers;

			
			overlay position: { 5, 5 } size: { 300 #px, 150 #px } background: #black transparency: 1.0 border: #black 
            {
            	
                rgb text_color<-#green;
                float y <- 10#px;
  				draw "number of people in:" at: { 5#px, 20#px } color: #black font: font("Helvetica", 17, #bold) perspective:false;
                y <- y + 35 #px;
  				draw "TD branches: " + tdpeople at: { 10#px, y } color: rgb(52, 132, 2) font: font("Helvetica", 15, #bold) perspective:false;
                y <- y + 30 #px;
                draw "CIBC branches: " + cibcpeople at: { 10#px, y } color: rgb(229, 65, 57) font: font("Helvetica", 15, #bold) perspective:false;
                y <- y + 30 #px;
                draw "BMO branches: " + bmopeople at: { 10#px, y } color: rgb(1, 93, 163) font: font("Helvetica", 15, #bold) perspective:false;
                y <- y + 30 #px;
                draw "Tim Horton's: " + thspeople at: { 10#px, y } color: rgb(135, 20, 2) font: font("Helvetica", 15, #bold) perspective:false;
                y <- y + 60 #px;
                draw "Total number of people: " + (thspeople+cibcpeople+bmopeople+thspeople) at: { 10#px, y } color: #black font: font("Helvetica", 15, #bold) perspective:false;
                y <- y + 50 #px;                
                draw "Month: August" at: { 10#px, y } color: rgb(1, 186, 183) font: font("Helvetica", 15, #bold) perspective:false;
                y <- y + 30 #px;
                if (day > 31){
                	day <- 31;
                }
                draw "Day:" + day at: { 10#px, y } color: rgb(1, 186, 183) font: font("Helvetica", 15, #bold) perspective:false;
            }
            //Toronto
//			chart "People Distribution" background:#white type: pie size: {0.3,0.35} position: {world.shape.width*0.70,world.shape.height*0.65} color: #black axes: #yellow title_font: 'Helvetica' title_font_size: 12.0 
//			tick_font: 'Helvetica' tick_font_size: 10 tick_font_style: 'bold' label_font: 'Helvetica' label_font_size: 32 label_font_style: 'bold' x_label: 'Nice Xlabel' y_label:'Nice Ylabel'
//			{
//
//				  data "TD" value: tdpeople color: rgb(52, 132, 2);
//				  data "CIBC" value:cibcpeople color:rgb(229, 65, 57);
//				  data "BMO" value:bmopeople color: rgb(1, 93, 163);
//				  data "TimHorton's" value:thspeople color: rgb(135, 20, 2);
//			}
                //Mississauga chart
//			chart "People Distribution" background:#white type: pie size: {0.3,0.35} position: {0,0} color: #black axes: #yellow title_font: 'Helvetica' title_font_size: 12.0 
//			tick_font: 'Helvetica' tick_font_size: 10 tick_font_style: 'bold' label_font: 'Helvetica' label_font_size: 32 label_font_style: 'bold' x_label: 'Nice Xlabel' y_label:'Nice Ylabel'
//			{
//
//				  data "TD" value: tdpeople color: rgb(52, 132, 2);
//				  data "CIBC" value:cibcpeople color:rgb(229, 65, 57);
//				  data "BMO" value:bmopeople color: rgb(1, 93, 163);
//				  data "TimHorton's" value:thspeople color: rgb(135, 20, 2);
//			}			

		}
	//monitor "Number of total people" value: length(users) ;		
//	monitor "total people in TD branches" value:nbofTD;
//	monitor "total people in CIBC branches" value:nbofCIBC;
//	monitor "total people in BMO branches" value:nbofBMO;
//	monitor "total people in TimHorton branches" value:nbofTHS;
	}

}