/**
* Name: dwellTime
* Author: farzin
* Description: 
* Tags: Tag1, Tag2, TagN
*/

model dwellTime

/* Insert your model definition here */

global {
	
	shape_file shape_canada <- shape_file("../includes/Buildings_TO_WGS84.shp");//shape_file("../includes/Route_Regional_Road.shp");//shape_file("../includes/2018_Vacant_Lands_with_PropertyPINS.shp");//
    geometry shape <- envelope(shape_canada);

    list<string> userDate <- [];
    list<string> userTime <- [];
    list<string> user_Ids <- [];
    string userId;
    int deviceType;
    string provinceName <- "";
    string DateTime <- "";
    list<point> user <- [];
    list<point> user_td <- [];
    int initOffset;
    list<td> tdBranches <- [];

    int nb_people;
    int dead_people;
    int nb_bmo_people;
	int numberOfPeopleInBranches;

	int tableId;
    	map<string, unknown> matrixData;
    	
	file tdfilename;
	file cibcfilename;
	file bmofilename;
	file thsfilename;
	
	int hour;
	string filename;
	list<people> tempPeople;
	geometry copyshape;	
	
	int day <- 0 parameter: "day";
	float t;

	init {
			initOffset <- 0;
			nb_people <- 0;
			dead_people <- 0;
			copyshape <- copy(shape);
			hour <- 8;//0;

		  	
		  	do createTDs;
		  	do createBMO;
		  	do createCIBC;
		  	do createTimHortons;
		  	

		  	day <- 10;		  	
			 filename <-"DowntownToronto3_201808" + day + ".csv";// "RichmondHill_201808" + day + ".csv";// "Mississauga_201808" + day + ".csv";
			
			 tdfilename <- file("../results/TD/"+filename);
			 cibcfilename <- file("../results/CIBC/"+filename);
			 bmofilename <- file("../results/BMO/"+filename);
			 thsfilename <- file("../results/THS/"+filename);
			 
		  	do createpeople(hour);
	  	
	}

		
	reflex name:repeat when: (cycle!= 0 and cycle mod 180 = 0){
		write "number of people= " + length(people);
		hour <- hour + 1;
		write "hour= " + hour;
		if hour > 23 {
			do pause;
		}
		else{
			write "else";

			do addtofiles();

			do createpeople(hour);
		}
	}
	reflex name:start when:cycle=0{
		write "cycle 0";
		do createpeople(hour);
	}

	action addtofiles{
			ask people{
				if self.dwell_time > 240{
					switch self.bank{
						match "TD"{
							save [id,userid,inbranch_location.x ,inbranch_location.y,user_date[length(user_date)-idx] + "-" + user_time[length(user_time)-idx],bankAddress,initLocation.x,initLocation.y,dwell_time] to: "../results/TD/"+filename type:"csv" rewrite:false;
						}
						match "BMO"{
							
							save [id,userid,inbranch_location.x,inbranch_location.y,user_date[length(user_date)-idx] + "-" + user_time[length(user_time)-idx],bankAddress,initLocation.x,initLocation.y,dwell_time] to: "../results/BMO/"+filename type:"csv" rewrite:false;						
						}
						match "CIBC"{
							
							save [id,userid,inbranch_location.x,inbranch_location.y,user_date[length(user_date)-idx] + "-" + user_time[length(user_time)-idx],bankAddress,initLocation.x,initLocation.y,dwell_time] to: "../results/CIBC/"+filename type:"csv" rewrite:false;						
						}
						match "THS"{
							
							save [id,userid,inbranch_location.x,inbranch_location.y,user_date[length(user_date)-idx] + "-" + user_time[length(user_time)-idx],bankAddress,initLocation.x,initLocation.y,dwell_time] to: "../results/THS/"+filename type:"csv" rewrite:false;						
						}
					}
					do die;
				}
			}			
		
	}


	action createpeople(int hr){
   		list<people> pep;
		write "reading csv file, " + day + " - " + hr;
		
		string fromAWStd_gama <- "../files/20188"+ day  + hr + ".csv";//  "http://localhost:8080/getCSVData/" + day + "/" + hr;
		if hr = 0 {
			fromAWStd_gama <- "../files/20188"+ day + ".csv";
		}
		

		write fromAWStd_gama;
		csv_file my_csv <- csv_file(fromAWStd_gama,",");
		matrix athenaData <- matrix(my_csv);
		userId <- athenaData[0,1];
		write "rows= " + athenaData.rows;
		write "people= " + length(people);
			
			
		loop r from: 1 to: athenaData.rows -1{
						float lng <- athenaData[4,r];
						float lat <- athenaData[3,r];
							if (userId != athenaData[0,r]){	
									create people{
										userid <- userId; 
										user_date <- userDate;
										user_time <- userTime;
										//users <- user;
										location <- user[0];
										initLocation <- user[0];
										agentMovementList <- user;	
										
										user_TD_location <- user_td;
									}
									userId <- athenaData[0,r];
									user <- [];
									userDate <- [];
									userTime <- [];
									user_td <- [];
									user <+ point(to_GAMA_CRS({lng, lat}, "EPSG:4326"));
									user_td <+ {lng, lat};
									DateTime <- athenaData[2,r];
									// we can use data type Date instead.
									list<string> date_time <- split_with(DateTime, " ");
									userDate <+ date_time[0];
									userTime <+ date_time[1];									
							}
							else{
									user <+ point(to_GAMA_CRS({lng, lat}, "EPSG:4326"));
									user_td <+ {lng, lat};
									DateTime <- athenaData[2,r];
									// we can use data type Date instead.
									list<string> date_time <- split_with(DateTime, " ");
									userDate <+ date_time[0];
									userTime <+ date_time[1];
									//date h <- date(date_time[1]);
									
							}
		}	


				  nb_people <- length(people);
				  write "length of people= " + nb_people;

}
		
	
// Create TD agents	
//972 Albion Rd Etobicoke ON M9V1A7 has wrong polygon coordinate. It's the biggest area on the map. I delete it from the database for now until we fix it
	action createTDs{
	
		map<string, unknown> cityMatrixData;
		string cityIOUrl <- "../includes/tdDowntownToronto2.json";
		list<map<string, unknown>> cityMatrixCell;
		list<float> density_array;
		list<float> current_density_array;	
		float lng;
		float lat;		
			cityMatrixData <- json_file(cityIOUrl).contents;
			cityMatrixCell <- cityMatrixData["candidates"];
			
			loop l over: cityMatrixCell { 
				lng <- float(l["location"]["x"]);
				lat  <- float(l["location"]["y"]);
				create td{
					address <- l["attributes"]["Place_addr"];
					loc <- point(to_GAMA_CRS({lng, lat}, "EPSG:4326"));
					location <- loc;
				}
				
			}
	}

	action destructTDs{
		ask td{
			do die;
		}
	}
	
	action createBMO{
		map<string, unknown> cityMatrixData;
		string cityIOUrl <- "../includes/bmoDowntownToronto2.json";
		list<map<string, unknown>> cityMatrixCell;
		list<float> density_array;
		list<float> current_density_array;	
		float lng;
		float lat;		
			cityMatrixData <- json_file(cityIOUrl).contents;
			cityMatrixCell <- cityMatrixData["candidates"];

			
			loop l over: cityMatrixCell { 
				lng <- float(l["location"]["x"]);
				lat  <- float(l["location"]["y"]);

				create bmo{
					address <- l["attributes"]["Place_addr"];
					loc <- point(to_GAMA_CRS({lng, lat}, "EPSG:4326"));
					location <- loc;
				}
				
			}		
		
	}
	
	action destroyBMO{
		ask bmo{
			do die;
		}
	}	
	
	action createCIBC{
		map<string, unknown> cityMatrixData;
		string cityIOUrl <- "../includes/cibcDowntownToronto2.json";	
		list<map<string, unknown>> cityMatrixCell;
		list<float> density_array;
		list<float> current_density_array;	
		float lng;
		float lat;		
			cityMatrixData <- json_file(cityIOUrl).contents;
			cityMatrixCell <- cityMatrixData["candidates"];

			
			loop l over: cityMatrixCell { 
				lng <- float(l["location"]["x"]);
				lat  <- float(l["location"]["y"]);

				create cibc{
					address <- l["attributes"]["Place_addr"];
					loc <- point(to_GAMA_CRS({lng, lat}, "EPSG:4326"));
					location <- loc;
				}
				
			}				
	}
	
	action destroyCIBC{
		ask cibc{
			do die;
		}
	}	
	
	action createTimHortons{
		map<string, unknown> cityMatrixData;
		string cityIOUrl <- "../includes/thsDowntownToronto2.json";
		list<map<string, unknown>> cityMatrixCell;
		list<float> density_array;
		list<float> current_density_array;	
		float lng;
		float lat;		
			cityMatrixData <- json_file(cityIOUrl).contents;
			cityMatrixCell <- cityMatrixData["candidates"];
			
			loop l over: cityMatrixCell { 
				lng <- float(l["location"]["x"]);
				lat  <- float(l["location"]["y"]);


				create timHortons{
					address <- l["attributes"]["Place_addr"];
					loc <- point(to_GAMA_CRS({lng, lat}, "EPSG:4326"));
					location <- loc;
				}
				
			}
	}
	
	action destroyTimHortons{
		ask timHortons{
			do die;
		}
	}

}


species name:toronto{
	geometry loc;
	int n_people;

	
	user_command cmd_inside_td {
		loc <- shape CRS_transform("EPSG:4326");

		list<people> people_ <- agents_at_distance(20) of_species people;

		n_people <- length(people_);
		
	}
	aspect name:base{
		draw shape color:#grey;
	}
	
}

species name:td{
	string address;
	geometry shape1;
	int numberOfPeople;
	image_file my_icon <- file("../includes/TD-bank-icon.png") ;	
	list<people> inside_branch;
	list<people> inside_branch_temp;
	list<people> lp;
	list<people> i;
	list<string> uid;
	geometry loc;	
	float diffDate;
	int n_people;

	user_command people_inside_branch {
		numberOfPeople <- length(agents_inside(self) of_species people);
		loc <- shape CRS_transform("EPSG:4326");
	}
	
	reflex name:peopleInsideTheBranch{
		lp <- agents_at_distance(10.0#m) of_species people; 
///		write "number of people= " + length(lp);
		loop p over:(lp) {
			p.notdeleted <- true;
			//add name to:b.banks;
			p.inbranch_location <- p.location CRS_transform("EPSG:4326");
			p.bankAddress <- replace(address,',','-');  //"BMO-" + name;
			p.bank <- "TD";				//write name + ",  cnt=" + p.cnt;
			if (p.bank_name != "" and p.bank_name != name){
				p.bank_name <- name;
				add p.dwell_time to:p.dwell_times;
				p.dwell_time <- 0.0;
			}	
			if (p.cnt != 0 and length(p.user_time) > 1){

				try{
					diffDate <- date(p.user_date[p.cnt]+"T" + p.user_time[p.cnt]) - date(p.user_date[p.cnt-1]+"T" + p.user_time[p.cnt-1]);//-d
				}
				catch{
					write " " + p.cnt + " - " + p.name + " - " + length(p.user_date);
				}
				//d <- date(p.user_date[p.cnt]+"T" + p.user_time[p.cnt]);
				p.dwell_time <- p.dwell_time + diffDate;
//				write "dwell= " + p.userid + " , " + p.dwell_time;
			}
		}		
		
//		i <- ((agents_at_distance(10.0#m) of_species people) where !(each.userid in uid));
//		uid <-  (i collect each.userid) + uid;
//
//		loop b over:(i) {
//			b.notdeleted <- true;
////			add name to:b.banks;
//			b.bank_name <- name;
//			b.inbranch_location <- b.location CRS_transform("EPSG:4326");
//			b.bank <- "TD";
//			b.bankAddress <- replace(address,',','-'); 
//			
//		}
	}

	aspect base {
		//draw shape color:#blue;
		draw my_icon size: 250.0;
	}
}

species name:bmo{
	float latitude;
	float longitude;
	string address;
	int n_bmo_people;
	int peopleInsideThisBranch;
	image_file bmo_icon <- file("../includes/bmo.jpg") ;
	list<people> lp;
	list<people> i;
	list<string> uid;
	geometry loc;	
	float diffDate;
	//date d <- date([2018,8,1,0,0,0]);//("2018-8-1T00:00:00:00");
	user_command peopleInsideThisBranch{
		peopleInsideThisBranch <- length(agents_at_distance(6.0#m) of_species people);		
	}
	
	reflex name:peopleInsideTheBranch{//} when:length(bmo) > 0{
		lp <- agents_at_distance(10.0#m) of_species people; 
///		write "number of people= " + length(lp);
		loop p over:(lp) {
			p.notdeleted <- true;
			//add name to:b.banks;
			p.inbranch_location <- p.location CRS_transform("EPSG:4326");
			p.bankAddress <- replace(address,',','-');  //"BMO-" + name;
			p.bank <- "BMO";				//write name + ",  cnt=" + p.cnt;
			if (p.bank_name != "" and p.bank_name != name){
				p.bank_name <- name;
				add p.dwell_time to:p.dwell_times;
				p.dwell_time <- 0.0;
			}	
			if (p.cnt != 0 and length(p.user_time) > 1){

				try{
					diffDate <- date(p.user_date[p.cnt]+"T" + p.user_time[p.cnt]) - date(p.user_date[p.cnt-1]+"T" + p.user_time[p.cnt-1]);//-d
				}
				catch{
					write " " + p.cnt + " - " + p.name + " - " + length(p.user_date);
				}
				//d <- date(p.user_date[p.cnt]+"T" + p.user_time[p.cnt]);
				p.dwell_time <- p.dwell_time + diffDate;
//				write "dwell= " + p.userid + " , " + p.dwell_time;
			}
		}		


	}	
	
	aspect name:base{
		draw bmo_icon size: 250.0;
	}	
}

species name:cibc{
	float latitude;
	float longitude;
	string address;
	int id;
	int n_bmo_people;
	int peopleInsideThisBranch;
	image_file cibc_people <- file("../includes/cibc.jpg") ;
	list<people> lp;
	list<people> i;
	list<string> uid;
	geometry loc;	
	float diffDate;	
	
	user_command peopleInsideThisBranch{
		peopleInsideThisBranch <- length(agents_at_distance(6.0#m) of_species people);		
	}
		
	
	reflex name:peopleInsideTheBranch when:length(cibc) > 0{
				lp <- agents_at_distance(10.0#m) of_species people; 
///		write "number of people= " + length(lp);
		loop p over:(lp) {
			p.notdeleted <- true;
			//add name to:b.banks;
			p.inbranch_location <- p.location CRS_transform("EPSG:4326");
			p.bankAddress <- replace(address,',','-');  //"BMO-" + name;
			p.bank <- "CIBC";				//write name + ",  cnt=" + p.cnt;
			if (p.bank_name != "" and p.bank_name != name){
				p.bank_name <- name;
				add p.dwell_time to:p.dwell_times;
				p.dwell_time <- 0.0;
			}	
			if (p.cnt != 0 and length(p.user_time) > 1){

				try{
					diffDate <- date(p.user_date[p.cnt]+"T" + p.user_time[p.cnt]) - date(p.user_date[p.cnt-1]+"T" + p.user_time[p.cnt-1]);//-d
				}
				catch{
					write " " + p.cnt + " - " + p.name + " - " + length(p.user_date);
				}
				//d <- date(p.user_date[p.cnt]+"T" + p.user_time[p.cnt]);
				p.dwell_time <- p.dwell_time + diffDate;
//				write "dwell= " + p.userid + " , " + p.dwell_time;
			}
		}		
		
//		i <- ((agents_at_distance(10.0#m) of_species people) where !(each.userid in uid));
//		uid <-  (i collect each.userid) + uid;
//		loop b over:(i) {
//			b.notdeleted <- true;
////			add name to:b.banks;
//			b.bank_name <- name;
//			b.inbranch_location <- b.location CRS_transform("EPSG:4326");
//			b.bankAddress <- replace(address,',','-'); 
//			b.bank <- "CIBC";
//		}

	}	
	
	aspect name:base{
		draw cibc_people size: 250.0;
	}	
}

species name:timHortons{
//	string city;
//	string state;
	float latitude;
	float longitude;
	string address;
	int peopleInsideThisTimHorton;
	image_file my_icon <- file("../includes/timhortons.png") ;
	list<people> lp;
	list<people> i;
	list<string> uid;
	geometry loc;	
	float diffDate;	
	user_command peopleInsideThisTimHortons{
		peopleInsideThisTimHorton <- length(agents_at_distance(6.0#m) of_species people);		
	}
	
	reflex name:peopleInsideTheBranch when:length(bmo) > 0{
				lp <- agents_at_distance(10.0#m) of_species people; 
///		write "number of people= " + length(lp);
		loop p over:(lp) {
			p.notdeleted <- true;
			//add name to:b.banks;
			p.inbranch_location <- p.location CRS_transform("EPSG:4326");
			p.bankAddress <- replace(address,',','-');  //"BMO-" + name;
			p.bank <- "THS";				//write name + ",  cnt=" + p.cnt;
			if (p.bank_name != "" and p.bank_name != name){
				p.bank_name <- name;
				add p.dwell_time to:p.dwell_times;
				p.dwell_time <- 0.0;
			}	
			if (p.cnt != 0 and length(p.user_time) > 1){

				try{
					diffDate <- date(p.user_date[p.cnt]+"T" + p.user_time[p.cnt]) - date(p.user_date[p.cnt-1]+"T" + p.user_time[p.cnt-1]);//-d
				}
				catch{
					write " " + p.cnt + " - " + p.name + " - " + length(p.user_date);
				}
				//d <- date(p.user_date[p.cnt]+"T" + p.user_time[p.cnt]);
				p.dwell_time <- p.dwell_time + diffDate;
//				write "dwell= " + p.userid + " , " + p.dwell_time;
			}
		}		
		
//		i <- ((agents_at_distance(10.0#m) of_species people) where !(each.userid in uid));
//		uid <-  (i collect each.userid) + uid;
//		loop b over:(i) {
//			b.notdeleted <- true;
////			add name to:b.banks;
//			b.bank_name <- name;
//			b.inbranch_location <- b.location CRS_transform("EPSG:4326");
//			b.bankAddress <- replace(address,',','-'); //b.bankName <- "THS-" + name;
//			b.bank <- "THS";
//		}
	}	
	
	aspect name:base{
		draw my_icon size: 225.0;
	}
}
species name:people skills:[moving]{
	list<point> agentMovementList <- [];
	point newLocation <- nil;
	list<string> user_date;
	list<string> user_time;
	list<point> users;
	list<point> user_TD_location;
	point inbranch_location;
	list<string> banks;	
	string userid;
	int id;
	float latitude;
	float longitude;
	bool notdeleted;
	string date_entered;
	string bankAddress;
	int idx <- 1;
	string bank;
	string bank_name;
	int cnt <- 0;
	float dwell_time;
	list<float> dwell_times;
	map<float,string> dwelltime_location;
	//image_file my_icon <- file("../includes/person.png") ;
	//int i <- 0;
	point initLocation;
	
	reflex name:removeagentMovementList{
		agentMovementList  <- agentMovementList - first(agentMovementList);
		
	}
	
	reflex name:dead when:empty(agentMovementList){
		// people agents that are not withinn a location should be removed 
		if notdeleted = false{
			do die;
		}

	}
	
	reflex name:move when:!empty(agentMovementList){//} and length(agentMovementList)-1>0{
		//write "people";
			cnt <- cnt + 1;
						idx <- length(agentMovementList);
			
						//agentMovementList  <- agentMovementList - first(agentMovementList);
			
			location <- first(agentMovementList);
//			idx <- length(agentMovementList);
//			agentMovementList  <- agentMovementList - first(agentMovementList);
		}		
	aspect name:base{
		draw shape color:#brown;// size: 250.0;
	}	
}



experiment default_expr type: gui{

	output{
		display dis_td{
			image ("../images/CITY-OF-TORONTO-map_rt.jpg") refresh: false;
			
					species td aspect:base refresh:false;
			species people aspect: base;
			species timHortons aspect:base refresh:false;
			species bmo aspect:base;
			species cibc aspect:base;
			species toronto aspect:base;
		}	
	}
	
}
