package main

import (
	"fmt"
	"net/http"
	"os"
	"strconv"
)

func main() {
	url := ""

	for d := 20; d < 32; d++ {
		for h := 0; h < 24; h++ {
			url = "http://localhost:8080/getCSVData/" + strconv.Itoa(d) + "/" + strconv.Itoa(h)
			_, err := http.Get(url)
			if err != nil {
				fmt.Printf("%s", err)
				os.Exit(1)
			}
		}
	}
}
