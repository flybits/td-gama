package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/athena"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/labstack/echo"
	minio "github.com/minio/minio-go"
)

type User struct {
	UserId    string `json:"userid"`
	TenantId  string `json:"tenantid"`
	UserTime  string `json:"time"`
	Latitude  string `json:"latitude"`
	Longitude string `json:"longitude"`
}

var users []User

// func userRouter(w http.ResponseWriter, req *http.Request) {
// 	AWS_ACCESS_KEY_ID := "*********************"                         
// 	AWS_SECRET_ACCESS_KEY := "************************************" 
// 	query := "SELECT userid,tenantid,from_unixtime(timestamp) as user_time,value.lat as latitude, value.lng as longitude FROM location WHERE partition_0='2018'	AND partition_1= '08' AND partition_2='01' AND partition_3='14' AND floor(value.lat) = 43 AND ceil(value.lng) = -79 LIMIT 10;"

// 	awscfg := &aws.Config{
// 		Region:      aws.String("us-east-1"),
// 		Credentials: credentials.NewStaticCredentials(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, ""),
// 	}
// 	awscfg.WithRegion("us-east-1")
// 	// Create the session that the service will use.
// 	sess := session.Must(session.NewSession(awscfg))

// 	svc := athena.New(sess, aws.NewConfig().WithRegion("us-east-1"))
// 	var s athena.StartQueryExecutionInput
// 	s.SetQueryString(query)

// 	var q athena.QueryExecutionContext
// 	q.SetDatabase("v2datalake")
// 	s.SetQueryExecutionContext(&q)

// 	var r athena.ResultConfiguration
// 	r.SetOutputLocation("s3://aws-athena-query-results-118253587892-us-east-1")
// 	s.SetResultConfiguration(&r)

// 	result, err := svc.StartQueryExecution(&s)
// 	if err != nil {
// 		fmt.Println(err)
// 		return
// 	}
// 	fmt.Println("StartQueryExecution result:")
// 	fmt.Fprintln(w, result.GoString())

// 	var qri athena.GetQueryExecutionInput
// 	qri.SetQueryExecutionId(*result.QueryExecutionId)

// 	var qrop *athena.GetQueryExecutionOutput
// 	duration := time.Duration(2) * time.Second // Pause for 2 seconds

// 	for {
// 		qrop, err = svc.GetQueryExecution(&qri)
// 		if err != nil {
// 			fmt.Println(err)
// 			return
// 		}
// 		if *qrop.QueryExecution.Status.State != "RUNNING" {
// 			break
// 		}
// 		fmt.Println("waiting.")
// 		time.Sleep(duration)

// 	}
// 	if *qrop.QueryExecution.Status.State == "SUCCEEDED" {

// 		var ip athena.GetQueryResultsInput
// 		ip.SetQueryExecutionId(*result.QueryExecutionId)

// 		_, err := svc.GetQueryResults(&ip)
// 		if err != nil {
// 			fmt.Println(err)
// 			return
// 		}
// 		//fmt.Printf("%+v", op)
// 		//fmt.Fprintln(w, op.ResultSet.Rows)

// 		// w.Header().Set("Content-Type", "text/csv")
// 		// w.Header().Set("Content-Disposition", "attachment;filename=945689fe-cc28-4dcb-af8e-ebf30f248313.csv")
// 		// wr := csv.NewWriter(w)

// 		bucket := "aws-athena-query-results-118253587892-us-east-1"
// 		item := "945689fe-cc28-4dcb-af8e-ebf30f248313.csv"

// 		file, err := os.Create("td.csv")
// 		if err != nil {

// 		}
// 		downloader := s3manager.NewDownloader(sess)
// 		_, err1 := downloader.Download(file,
// 			&s3.GetObjectInput{
// 				Bucket: aws.String(bucket),
// 				Key:    aws.String(item),
// 			})

// 		if err1 != nil {
// 			log.Fatalf("Unable to download item %q, %v", item, err1)
// 		}

// 		// err := wr.Write()
// 		// if err != nil {
// 		// 	http.Error(w, "Error sending csv: "+err.Error(), http.StatusInternalServerError)
// 		// 	return
// 		// }
// 		//fmt.Fprintln(w, file)

// 		Filename := "td.csv"
// 		//Check if file exists and open
// 		Openfile, err := os.Open(Filename)
// 		defer Openfile.Close() //Close after function return
// 		if err != nil {
// 			//File not found, send 404
// 			http.Error(w, "File not found.", 404)
// 			return
// 		}
// 		//File is found, create and send the correct headers

// 		//Get the Content-Type of the file
// 		//Create a buffer to store the header of the file in
// 		FileHeader := make([]byte, 512)
// 		//Copy the headers into the FileHeader buffer
// 		Openfile.Read(FileHeader)
// 		//Get content type of file
// 		FileContentType := http.DetectContentType(FileHeader)

// 		//Get the file size
// 		FileStat, _ := Openfile.Stat()                     //Get info from file
// 		FileSize := strconv.FormatInt(FileStat.Size(), 10) //Get file size as a string

// 		//Send the headers
// 		w.Header().Set("Content-Disposition", "attachment; filename="+Filename)
// 		w.Header().Set("Content-Type", FileContentType)
// 		w.Header().Set("Content-Length", FileSize)

// 		//Send the file
// 		//We read 512 bytes from the file already so we reset the offset back to 0
// 		Openfile.Seek(0, 0)
// 		io.Copy(w, Openfile) //'Copy' the file to the client
// 		return

// 		//fmt.Fprintln(w, "s3://aws-athena-query-results-118253587892-us-east-1/945689fe-cc28-4dcb-af8e-ebf30f248313.csv")
// 		//, "{\"Rows\": [{\"Data\": [{ \"VarCharValue\": \"userid\"},{\"VarCharValue\": \"tenantid\"},{\"VarCharValue\": \"user_time\"},{\"VarCharValue\": \"latitude\"},{\"VarCharValue\": \"longitude\"}]},{\"Data\":[{\"VarCharValue\": \"9130DDED-0DDE-47E8-B831-79DF260F8B3C\"},{\"VarCharValue\": \"D94FA9CB-42C4-4B33-A68A-3CE5FF0EC186\"},{\"VarCharValue\": \"2018-08-01 09:27:37.000\"},{\"VarCharValue\": \"43.6994059\"},{\"VarCharValue\": \"-79.3901954\"}]}]}")
// 	} else {
// 		fmt.Println(*qrop.QueryExecution.Status.State)
// 	}
// }

func main() {
	// fmt.Println("Starting JSON server")
	// http.HandleFunc("/user", userRouter)
	// http.ListenAndServe(":8080", nil)
	port := "8080"
	router := echo.New()
	AWS_ACCESS_KEY_ID := "***********************"                         
	AWS_SECRET_ACCESS_KEY := "**************************************" 
	endpoint := "s3.amazonaws.com"                                      
	useSSL := true
	id := ""
	//athenaBucket := "td-gama"

	router.GET("/:year/:month/:day/:hour", func(c echo.Context) error {
		year := c.ParamValues()[0]
		month := c.ParamValues()[1]
		day := c.ParamValues()[2]
		hour := c.ParamValues()[3]
		fmt.Println(day, hour)

		fmt.Println("inside get")

		query := "SELECT userid,tenantid,from_unixtime(timestamp) as user_time,value.lat as latitude, value.lng as longitude FROM location WHERE partition_0='" + year + "'	AND partition_1= '" + month + "' AND partition_2='" + day + "' AND partition_3='" + hour + "' AND floor(value.lat) = 43 AND ceil(value.lng) = -79 ORDER BY userid,from_unixtime(timestamp);"

		awscfg := &aws.Config{
			Region:      aws.String("us-east-1"),
			Credentials: credentials.NewStaticCredentials(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, ""),
		}
		awscfg.WithRegion("us-east-1")

		// Create the session that the service will use.
		sess := session.Must(session.NewSession(awscfg))

		svc := athena.New(sess, aws.NewConfig().WithRegion("us-east-1"))
		var s athena.StartQueryExecutionInput

		s.SetQueryString(query)

		var q athena.QueryExecutionContext
		q.SetDatabase("v2datalake")
		s.SetQueryExecutionContext(&q)

		var r athena.ResultConfiguration
		r.SetOutputLocation("s3://aws-athena-query-results-118253587892-us-east-1")
		s.SetResultConfiguration(&r)

		result, err := svc.StartQueryExecution(&s)
		if err != nil {
			fmt.Println(err)
			return err
		}

		fmt.Println("StartQueryExecution result:")
		//fmt.Fprintln(w, result.GoString())

		var qri athena.GetQueryExecutionInput

		qri.SetQueryExecutionId(*result.QueryExecutionId)
		//qri.SetQueryExecutionId("Gama" + year + month + day + hour)
		var qrop *athena.GetQueryExecutionOutput
		duration := time.Duration(1) * time.Second // Pause for 2 seconds

		for {
			qrop, err = svc.GetQueryExecution(&qri)
			if err != nil {
				fmt.Println(err)
				return err
			}
			if *qrop.QueryExecution.Status.State != "RUNNING" {
				break
			}
			fmt.Println("waiting.")
			time.Sleep(duration)
		}
		if *qrop.QueryExecution.Status.State == "SUCCEEDED" {

			// var ip athena.GetQueryResultsInput
			// ip.SetQueryExecutionId(*result.QueryExecutionId)
			// ////o := 0
			// ////for {

			// op, err := svc.GetQueryResults(&ip)
			// if err != nil {
			// 	fmt.Println(err)
			// 	return err
			// }
			// fmt.Printf("%+v", op)
			id = *result.QueryExecutionId
			fmt.Printf("ID= %s ", id)

			s3Obj := s3.New(sess, aws.NewConfig().WithRegion("us-east-1"))
			dest := year + strings.TrimLeft(month, "0") + strings.TrimLeft(day, "0") + strings.TrimLeft(hour, "0")
			fmt.Printf("dest= %s", dest)
			fmt.Println()

			input := &s3.CopyObjectInput{
				Bucket:     aws.String("td-gama"),
				CopySource: aws.String("/aws-athena-query-results-118253587892-us-east-1/" + id + ".csv"),
				Key:        aws.String("9/1/" + dest + ".csv"),
			}
			resCopy, err := s3Obj.CopyObject(input)
			if err != nil {
				fmt.Printf("errCopy= %v", err)
				return err
			}
			fmt.Printf("result of copy= %s", resCopy)

			inputRemove := &s3.DeleteObjectInput{
				Bucket: aws.String("aws-athena-query-results-118253587892-us-east-1"),
				Key:    aws.String(id + ".csv"),
			}
			resRemove, err := s3Obj.DeleteObject(inputRemove)
			if err != nil {
				fmt.Printf("errRemove= %v", err)
				return err
			}
			fmt.Printf("result of remove= %s", resRemove)

			//"s3://aws-athena-query-results-118253587892-us-east-1"
			/*
				//if *op.NextToken == "" || o == 5 {
				//	break
				//}
				//o++
				fmt.Println(*op.NextToken)
				ip.SetNextToken(*op.NextToken)
				//i := 0
				//var u user
				fmt.Println(len(op.ResultSet.Rows))

				for ridx, row := range op.ResultSet.Rows {
					if ridx > 0 {
						var u User
						for idx, d := range row.Data {
							//fmt.Println(idx)
							switch idx {
							case 0:
								u.UserId = *d.VarCharValue
							case 1:
								u.TenantId = *d.VarCharValue
							case 2:
								u.UserTime = *d.VarCharValue
							case 3:
								u.Latitude = *d.VarCharValue
							case 4:
								u.Longitude = *d.VarCharValue
							default:
								fmt.Println("no value")
							}
							//i++
						}
						users = append(users, u)
						//i = 0
					}
				}
				//}
				fmt.Println(len(users))
				//return c.JSON(http.StatusOK, users) //op.ResultSet.Rows
			*/
		} else {
			fmt.Printf("error1= %s ", err)
			return err
		}
		//}

		return nil
	})

	router.GET("/getCSVData/:day/:hour", func(c echo.Context) error {
		// Initialize minio client object.

		day := c.ParamValues()[0]
		hour := c.ParamValues()[1]
		fmt.Println("inside the second GET")
		minioClient, err := minio.New(endpoint, AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, useSSL)
		if err != nil {
			fmt.Printf("error1= %s ", err)
			return err
		}
		// object, err2 := minioClient.GetObject("aws-athena-query-results-118253587892-us-east-1", "", minio.GetObjectOptions{}) //minioClient.GetObject("coo-cameras", "0000d3a9-111a-11e7-a62a-02420af8010e.jpg", minio.GetObjectOptions{}) //
		// object, err2 := minioClient.GetObject("aws-athena-query-results-118253587892-us-east-1", id+".csv", minio.GetObjectOptions{}) //minioClient.GetObject("coo-cameras", "0000d3a9-111a-11e7-a62a-02420af8010e.jpg", minio.GetObjectOptions{}) //
		object, err2 := minioClient.GetObject("td-gama", "20188"+strings.TrimLeft(day, "0")+strings.TrimLeft(hour, "0")+".csv", minio.GetObjectOptions{}) //minioClient.GetObject("coo-cameras", "0000d3a9-111a-11e7-a62a-02420af8010e.jpg", minio.GetObjectOptions{}) //

		if err2 != nil {
			fmt.Printf("error2= %s", err2)
			return err2
		}

		// localFile, errr := os.Create("../local-file.csv")
		// if errr != nil {
		// 	fmt.Printf("error3= %s", errr)
		// 	return errr
		// }
		// if _, mess := io.Copy(localFile, object); mess != nil {
		// 	fmt.Printf("error4= %s", mess)
		// 	return mess
		// }
		fmt.Println(day + " - " + hour)

		localFile, err := os.Create("/Users/farzin/flybits/GAMA/gama_workspace6/TD4Me/files/" + "20188" + strings.TrimLeft(day, "0") + strings.TrimLeft(hour, "0") + ".csv")
		if err != nil {
			fmt.Println(err)
			return err
		}
		if _, err = io.Copy(localFile, object); err != nil {
			fmt.Println(err)
			return err
		}

		return nil
		//return c.Stream(200, "text/csv", object)

	})

	//router.POST("/people", func(c echo.Context) error {
	// Initialize minio client object.
	// minioClient, err := minio.New(endpoint, AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, useSSL)
	// if err != nil {
	// 	fmt.Printf("error1= %s ", err)
	// 	return err
	// }

	// object, err2 := minioClient.PutObject("td-gama", "123.csv", minio.GetObjectOptions{}) //minioClient.GetObject("coo-cameras", "0000d3a9-111a-11e7-a62a-02420af8010e.jpg", minio.GetObjectOptions{}) //

	// if err2 != nil {
	// 	fmt.Printf("error2= %s", err2)
	// 	return err2
	// }

	//fmt.Printf("%v", c.Request().Body)
	//return nil

	//})
	log.Fatal(router.Start(":" + port))
}
